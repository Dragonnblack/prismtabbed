﻿using System.Diagnostics;
using System.Windows.Input;
using Prism.Commands;
using Prism.Modularity;
using Prism.Mvvm;
using Prism.Navigation;

namespace PrismTabNav1.Views
{
    public class FirstPageViewModel : BindableBase, INavigationAware
    {
        private readonly INavigationService _navigationService;
        public DelegateCommand NavigateToTabContainerPageCommand { get; set; }

        public FirstPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            NavigateToTabContainerPageCommand = new DelegateCommand(NavigateToTabContainerPage);
            Title = "Main Page";
        }

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private void NavigateToTabContainerPage()
        {
            Debug.WriteLine($"**** Navigate to {nameof(TabContainerPage)}");
            _navigationService.NavigateAsync($"{nameof(TabContainerPage)}");
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
            Debug.WriteLine($"{this.GetType().Name}.{nameof(OnNavigatedFrom)}");
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            Debug.WriteLine($"{this.GetType().Name}.{nameof(OnNavigatedTo)}");
        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {
            Debug.WriteLine($"{this.GetType().Name}.{nameof(OnNavigatingTo)}");
        }
    }
}